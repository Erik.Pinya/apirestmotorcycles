const express = require("express"); 
const cors = require("cors"); 
const bodyParser = require("body-parser"); 

const app = express();
const baseUrl = "/motorcycles";
app.use(cors());

app.use(bodyParser.urlencoded({ extended: false })); 
app.use(bodyParser.json());

ddbbConfig = {
  user: "erik-pinya-7e3",
  host: "postgresql-erik-pinya-7e3.alwaysdata.net",
  database: "erik-pinya-7e3_motorcycles",
  password: "contra2199",
  port: 5432,
};

const Pool = require("pg").Pool;
const pool = new Pool(ddbbConfig);


app.get(baseUrl + "/test", getTest);

const getMotos = (request, response) => {
  let brand = request.query.marca || "";

  const capitalizeFirstLetter = (motorcycleBrand) => {
    return motorcycleBrand.charAt(0).toUpperCase() + motorcycleBrand.slice(1);
  };

  let myQuery;
  console.log(brand);
  if (brand === "") {
    myQuery = "SELECT * FROM  motos;";
  } else {
    myQuery = `SELECT * FROM  motos WHERE marca='${capitalizeFirstLetter(
      brand
    )}';`;
  }

  pool.query(myQuery, (error, results) => {
    if (error) {
      throw error;
    }
    response.status(200).json(results.rows);
  });
};
app.get(baseUrl + "/getmotos", getMotos);

const deleteMoto = (request, response) => {
  const motorcycleId = request.params.id;

  var consulta = `DELETE FROM motos WHERE id=${motorcycleId};`;
  pool.query(consulta, (error, results) => {
    if (error) {
      throw error;
    }
    if (results.rowCount != 0) {
      response.status(200).json(results.rows);
    } else {
      response.status(200).json({ response: "No existeix la moto" });
    }
  });
};


const addMoto = (request, response) => {
  console.log(request.body);
  response.send("lo que llega es" + JSON.stringify(request.body));
};

app.post(baseUrl + "/moto", addMoto);
app.delete(baseUrl + "/deleteMoto/:id", deleteMoto);


const PORT = process.env.PORT || 3000; 
const IP = process.env.IP || null; 

app.listen(PORT, IP, () => {
  console.log("puerto del servidor" + PORT);
});
